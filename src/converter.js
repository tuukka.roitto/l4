const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex );
}

module.exports = {
    rgbToHex: (red, green, blue) => {
        const redHex = red.toString(16); // ff
        const greenHex = green.toString(16);
        const blueHex = blue.toString(16);
        return pad(redHex) + pad(greenHex) + pad(blueHex); // ff0000
    },
     
    hexToRgb: ("hex", (hex) => {
        const rgbInt = parseInt(hex, 16);
        const redRgb = (rgbInt >> 16) & 255;
        const greenRgb = (rgbInt >> 8) & 255;
        const blueRgb = (rgbInt & 255);
        

        return redRgb + "," + greenRgb + "," + blueRgb
    })
}